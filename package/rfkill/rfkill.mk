################################################################################
#
# rfkill
#
################################################################################

RFKILL_VERSION = 0.5
RFKILL_SOURCE = rfkill_$(RFKILL_VERSION).orig.tar.xz
RFKILL_SITE = http://http.debian.net/debian/pool/main/r/rfkill
RFKILL_LICENSE = GPLv2+ LGPLv2.1+
RFKILL_LICENSE_FILES = COPYING
#~ RFKILL_INSTALL_STAGING = YES

define RFKILL_BUILD_CMDS
	$(TARGET_MAKE_ENV)
		$(MAKE) -C $(@D) $(TARGET_CONFIGURE_OPTS)
endef

define RFKILL_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) \
		PREFIX=$(RFKILL_TARGET_DIR)/usr -C $(@D) install
endef

#~ RFKILL_CONF_ENV = ac_cv_prog_swig_found=no
#~ RFKILL_CONF_OPTS = --without-python

$(eval $(generic-package))
