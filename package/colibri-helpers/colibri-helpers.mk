################################################################################
#
# colibri-helpers
#
################################################################################

ifeq ($(COLIBRI_LOCAL_DEVELOPMENT),y)
COLIBRI_HELPERS_VERSION=local
COLIBRI_HELPERS_SITE=$(BR2_EXTERNAL)/../colibri-helpers
COLIBRI_HELPERS_SITE_METHOD = local
else
COLIBRI_HELPERS_SITE = $(call github,Colibri-Embedded,earlyboot-utils,$(COLIBRI_HELPERS_VERSION))
COLIBRI_HELPERS_VERSION = d63101264767b7fbf3f28c57310cbe08d816c4c2
#~ COLIBRI_HELPERS_VERSION = 1.0
#~ COLIBRI_HELPERS_SITE= https://gitlab.com/colibri-embedded/colibri-helpers/repository/v$(COLIBRI_HELPERS_VERSION)/archive.tar.gz
#~ COLIBRI_HELPERS_SOURCE=earlyboot-utils-v$(COLIBRI_HELPERS_VERSION).tar.gz
#~ COLIBRI_HELPERS_DL_OPTS=-O $(COLIBRI_HELPERS_SOURCE)
endif

COLIBRI_HELPERS_LICENSE = GPLv2
COLIBRI_HELPERS_LICENSE_FILES = LICENSE

define COLIBRI_HELPERS_BUILD_CMDS
	$(MAKE1) -C $(@D) CC="$(TARGET_CC)" DESTDIR=$(COLIBRI_HELPERS_TARGET_DIR)
endef 

define COLIBRI_HELPERS_INSTALL_TARGET_CMDS
	$(COLIBRI_HELPERS_FAKEROOT) -- $(MAKE1) -C $(@D) DESTDIR=$(COLIBRI_HELPERS_TARGET_DIR) install
endef

$(eval $(generic-package))
