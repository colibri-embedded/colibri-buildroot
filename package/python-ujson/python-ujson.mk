################################################################################
#
# python-ujson
#
################################################################################
PYTHON_UJSON_VERSION = 1.35
PYTHON_UJSON_SOURCE = ujson-$(PYTHON_UJSON_VERSION).tar.gz
PYTHON_UJSON_SITE = https://files.pythonhosted.org/packages/16/c4/79f3409bc710559015464e5f49b9879430d8f87498ecdc335899732e5377
PYTHON_UJSON_LICENSE = ESN
PYTHON_UJSON_LICENSE_FILES = LICENSE.txt
PYTHON_UJSON_SETUP_TYPE = setuptools

$(eval $(python-package))
