################################################################################
#
# bluez5_utils
#
################################################################################

BLUEZ5_UTILS_VERSION = 5.47
BLUEZ5_UTILS_SOURCE = bluez-$(BLUEZ5_UTILS_VERSION).tar.xz
BLUEZ5_UTILS_SITE = $(BR2_KERNEL_MIRROR)/linux/bluetooth
BLUEZ5_UTILS_INSTALL_STAGING = YES
BLUEZ5_UTILS_DEPENDENCIES = dbus libglib2
BLUEZ5_UTILS_LICENSE = GPL-2.0+, LGPL-2.1+
BLUEZ5_UTILS_LICENSE_FILES = COPYING COPYING.LIB

BLUEZ5_UTILS_CONF_OPTS = \
	--enable-tools \
	--enable-library \
	--disable-cups

ifeq ($(BR2_PACKAGE_BLUEZ5_UTILS_OBEX),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-obex
BLUEZ5_UTILS_DEPENDENCIES += libical
else
BLUEZ5_UTILS_CONF_OPTS += --disable-obex
endif

ifeq ($(BR2_PACKAGE_BLUEZ5_UTILS_CLIENT),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-client
BLUEZ5_UTILS_DEPENDENCIES += readline
else
BLUEZ5_UTILS_CONF_OPTS += --disable-client
endif

# experimental plugins
ifeq ($(BR2_PACKAGE_BLUEZ5_UTILS_EXPERIMENTAL),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-experimental
else
BLUEZ5_UTILS_CONF_OPTS += --disable-experimental
endif

# enable health plugin
ifeq ($(BR2_PACKAGE_BLUEZ5_PLUGINS_HEALTH),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-health
else
BLUEZ5_UTILS_CONF_OPTS += --disable-health
endif

# enable midi profile
ifeq ($(BR2_PACKAGE_BLUEZ5_PLUGINS_MIDI),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-midi
BLUEZ5_UTILS_DEPENDENCIES += alsa-lib
else
BLUEZ5_UTILS_CONF_OPTS += --disable-midi
endif

# enable nfc plugin
ifeq ($(BR2_PACKAGE_BLUEZ5_PLUGINS_NFC),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-nfc
else
BLUEZ5_UTILS_CONF_OPTS += --disable-nfc
endif

# enable sap plugin
ifeq ($(BR2_PACKAGE_BLUEZ5_PLUGINS_SAP),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-sap
else
BLUEZ5_UTILS_CONF_OPTS += --disable-sap
endif

# enable sixaxis plugin
ifeq ($(BR2_PACKAGE_BLUEZ5_PLUGINS_SIXAXIS),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-sixaxis
else
BLUEZ5_UTILS_CONF_OPTS += --disable-sixaxis
endif

# install gatttool (For some reason upstream choose not to do it by default)
ifeq ($(BR2_PACKAGE_BLUEZ5_UTILS_DEPRECATED),y)
define BLUEZ5_UTILS_INSTALL_GATTTOOL
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/attrib/gatttool $(BLUEZ5_UTILS_TARGET_DIR)/usr/bin/gatttool
endef
BLUEZ5_UTILS_POST_INSTALL_TARGET_HOOKS += BLUEZ5_UTILS_INSTALL_GATTTOOL
BLUEZ5_UTILS_CONF_OPTS += --enable-deprecated
else
BLUEZ5_UTILS_CONF_OPTS += --disable-deprecated
endif

# enable test
ifeq ($(BR2_PACKAGE_BLUEZ5_UTILS_TEST),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-test
else
BLUEZ5_UTILS_CONF_OPTS += --disable-test
endif

# use udev if available
ifeq ($(BR2_PACKAGE_HAS_UDEV),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-udev
BLUEZ5_UTILS_DEPENDENCIES += udev
else
BLUEZ5_UTILS_CONF_OPTS += --disable-udev
endif

# integrate with systemd if available
ifeq ($(BR2_PACKAGE_SYSTEMD),y)
BLUEZ5_UTILS_CONF_OPTS += --enable-systemd
BLUEZ5_UTILS_DEPENDENCIES += systemd
else
BLUEZ5_UTILS_CONF_OPTS += --disable-systemd
endif

define BLUEZ5_UTILS_SBIN_LINKS
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -d -m 0755 $(BLUEZ5_UTILS_TARGET_DIR)/usr/sbin
	$(BLUEZ5_UTILS_FAKEROOT) ln -sf /usr/libexec/bluetooth/bluetoothd $(BLUEZ5_UTILS_TARGET_DIR)/usr/sbin/bluetoothd
	$(BLUEZ5_UTILS_FAKEROOT) ln -sf /usr/libexec/bluetooth/obexd $(BLUEZ5_UTILS_TARGET_DIR)/usr/sbin/obexd
endef

define BLUEZ5_UTILS_CONFIG_FILES
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -d -m 0755 $(BLUEZ5_UTILS_TARGET_DIR)/etc/bluetooth
	$(BLUEZ5_UTILS_FAKEROOT) cp $(@D)/src/main.conf $(BLUEZ5_UTILS_TARGET_DIR)/etc/bluetooth
	$(BLUEZ5_UTILS_FAKEROOT) cp $(@D)/profiles/network/network.conf $(BLUEZ5_UTILS_TARGET_DIR)/etc/bluetooth
	$(BLUEZ5_UTILS_FAKEROOT) cp $(@D)/profiles/input/input.conf $(BLUEZ5_UTILS_TARGET_DIR)/etc/bluetooth
	$(BLUEZ5_UTILS_FAKEROOT) cp package/bluez5_utils/uart.conf $(BLUEZ5_UTILS_TARGET_DIR)/etc/bluetooth
endef

# bccmd, bdaddr, btinfo, btconfig, btmon, ciptool, obexctl.
ifeq ($(BR2_PACKAGE_BLUEZ5_UTILS_EXTRA),y)
define BLUEZ5_UTILS_EXTRA_TOOLS
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/tools/bccmd $(BLUEZ5_UTILS_TARGET_DIR)/usr/bin/bccmd
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/tools/bdaddr $(BLUEZ5_UTILS_TARGET_DIR)/usr/bin/bdaddr
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/tools/btinfo $(BLUEZ5_UTILS_TARGET_DIR)/usr/bin/btinfo
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/tools/btconfig $(BLUEZ5_UTILS_TARGET_DIR)/usr/bin/btconfig
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/tools/ciptool $(BLUEZ5_UTILS_TARGET_DIR)/usr/bin/ciptool
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/tools/obexctl $(BLUEZ5_UTILS_TARGET_DIR)/usr/bin/obexctl
endef

endif

BLUEZ5_UTILS_POST_INSTALL_TARGET_HOOKS += BLUEZ5_UTILS_SBIN_LINKS
BLUEZ5_UTILS_POST_INSTALL_TARGET_HOOKS += BLUEZ5_UTILS_CONFIG_FILES
BLUEZ5_UTILS_POST_INSTALL_TARGET_HOOKS += BLUEZ5_UTILS_EXTRA_TOOLS

define BLUEZ5_UTILS_INSTALL_INIT_SYSTEMD
	$(BLUEZ5_UTILS_FAKEROOT) mkdir -p $(TARGET_DIR)/etc/systemd/system/bluetooth.target.wants
	$(BLUEZ5_UTILS_FAKEROOT) ln -fs ../../../../usr/lib/systemd/system/bluetooth.service \
		$(TARGET_DIR)/etc/systemd/system/bluetooth.target.wants/bluetooth.service
	$(BLUEZ5_UTILS_FAKEROOT) ln -fs ../../../../usr/lib/systemd/system/bluetooth.service \
		$(TARGET_DIR)/etc/systemd/system/dbus-org.bluez.service
endef

define BLUEZ5_UTILS_INSTALL_INIT_SYSV
		
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0755 package/bluez5_utils/bluetooth.init \
		$(BLUEZ5_UTILS_TARGET_DIR)/etc/init.d/bluetooth
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -D -m 0644 package/bluez5_utils/bluetooth.default \
		$(BLUEZ5_UTILS_TARGET_DIR)/etc/default/bluetooth
		
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -d -m 0755 $(BLUEZ5_UTILS_TARGET_DIR)/etc/rc.d/rc.sysinit.d
	$(BLUEZ5_UTILS_FAKEROOT) $(INSTALL) -d -m 0755 $(BLUEZ5_UTILS_TARGET_DIR)/etc/rc.d/rc.shutdown.d
	
	$(BLUEZ5_UTILS_FAKEROOT) ln -fs ../../init.d/bluetooth \
		$(BLUEZ5_UTILS_TARGET_DIR)/etc/rc.d/rc.sysinit.d/S95bluetooth
	$(BLUEZ5_UTILS_FAKEROOT) ln -fs ../../init.d/bluetooth \
		$(BLUEZ5_UTILS_TARGET_DIR)/etc/rc.d/rc.shutdown.d/S50bluetooth
endef

$(eval $(autotools-package))
