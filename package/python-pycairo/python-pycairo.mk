################################################################################
#
# python-pycairo
#
################################################################################

PYTHON_PYCAIRO_VERSION = 1.15.3
PYTHON_PYCAIRO_SOURCE = pycairo-$(PYTHON_PYCAIRO_VERSION).tar.gz
PYTHON_PYCAIRO_SITE = https://github.com/pygobject/pycairo/releases/download/v$(PYTHON_PYCAIRO_VERSION)/
PYTHON_PYCAIRO_LICENSE = LGPL
PYTHON_PYCAIRO_LICENSE_FILES = COPYING-LGPL-2.1
PYTHON_PYCAIRO_SETUP_TYPE = distutils

PYTHON_PYCAIRO_DEPENDENCIES = cairo libsvg-cairo

$(eval $(python-package))
