################################################################################
#
# wampcc
#
################################################################################

WAMPCC_VERSION = 1.6
#WAMPCC_VERSION = be447e129acf14b311bbb90c15e9d52cb17d8cdc
#WAMPCC_SITE = $(call github,infinity0n3,wampcc,$(WAMPCC_VERSION))
#WAMPCC_SITE = $(call github,darrenjs,wampcc,$(WAMPCC_VERSION))
#https://github.com/darrenjs/wampcc/archive/v1.6.tar.gz
WAMPCC_SITE = https://github.com/darrenjs/wampcc/archive/
WAMPCC_SOURCE = v$(WAMPCC_VERSION).tar.gz
WAMPCC_LICENSE = MIT
WAMPCC_LICENSE_FILES = LICENSE
WAMPCC_INSTALL_STAGING = YES
WAMPCC_DEPENDENCIES = jansson openssl libuv

$(eval $(cmake-package))
