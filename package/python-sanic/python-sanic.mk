################################################################################
#
# python-sanic
#
################################################################################
PYTHON_SANIC_VERSION = 0.7.0
PYTHON_SANIC_SOURCE = sanic-$(PYTHON_SANIC_VERSION).tar.gz
PYTHON_SANIC_SITE = https://files.pythonhosted.org/packages/c1/d4/a6fa3efa7aa8ff6fb08cddac6f10e92da95e1fd44d6397fb481bf012c2a0
PYTHON_SANIC_LICENSE = MIT
PYTHON_SANIC_LICENSE_FILES = LICENSE
PYTHON_SANIC_SETUP_TYPE = setuptools
PYTHON_SANIC_DEPENDENCIES = python-uvloop python-websockets python-aiofiles python-httptools python-ujson

$(eval $(python-package))
