################################################################################
#
# iptraf-ng
#
################################################################################

#http://http.debian.net/debian/pool/main/i/iptraf-ng/iptraf-ng_1.1.4.orig.tar.gz

IPTRAF_NG_VERSION = 1.1.4
IPTRAF_NG_SOURCE = iptraf-ng_$(IPTRAF_NG_VERSION).orig.tar.gz
IPTRAF_NG_SITE = http://http.debian.net/debian/pool/main/i/iptraf-ng
IPTRAF_NG_LICENSE = GPLv2+
IPTRAF_NG_LICENSE_FILES = LICENSE
IPTRAF_NG_DEPENDENCIES = ncurses

IPTRAF_NG_MAKE_ENV = NCURSES_LDFLAGS="-lpanel -lncurses"

$(eval $(autotools-package))
