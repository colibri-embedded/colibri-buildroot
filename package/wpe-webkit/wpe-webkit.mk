#https://wpewebkit.org/releases/wpewebkit-2.20.2.tar.xz
################################################################################
#
# wpe-webkit
#
################################################################################

WPE_WEBKIT_VERSION = 2.20.2
WPE_WEBKIT_SITE = https://wpewebkit.org/releases
WPE_WEBKIT_SOURCE = wpewebkit-$(WPE_WEBKIT_VERSION).tar.xz
#WPE_WEBKIT_LICENSE = BSD-3c
#WPE_WEBKIT_LICENSE_FILES = LICENSE
#WPE_WEBKIT_SUPPORTS_IN_SOURCE_BUILD = NO

WPE_WEBKIT_CONF_OPTS += \
	-DPORT=WPE
#~ 	-DBUILD_WITH_DEBUG_INFO=OFF \
#~ 	-DBUILD_PERF_TESTS=$(if $(BR2_PACKAGE_OPENCV_BUILD_PERF_TESTS),ON,OFF) \
#~ 	-DBUILD_TESTS=$(if $(BR2_PACKAGE_OPENCV_BUILD_TESTS),ON,OFF)

$(eval $(cmake-package))
