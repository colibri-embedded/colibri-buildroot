################################################################################
#
# python-pybluez
#
################################################################################

PYTHON_PYBLUEZ_VERSION = b58f5bfd45d5ffa1d4315e973fde1bdd6a3e5c5b
PYTHON_PYBLUEZ_SITE = $(call github,karulis,pybluez,$(PYTHON_PYBLUEZ_VERSION))

PYTHON_PYBLUEZ_SETUP_TYPE = setuptools
PYTHON_PYBLUEZ_LICENSE = GPLv3
PYTHON_PYBLUEZ_LICENSE_FILES = COPYING
PYTHON_PYBLUEZ_DEPENDENCIES = bluez5_utils

$(eval $(python-package))
$(eval $(host-python-package))
