################################################################################
#
# triggerhappy
#
################################################################################

TRIGGERHAPPY_VERSION = aac9f353a28c0f414b27ac54bbbb2292c152eedc
TRIGGERHAPPY_SITE = $(call github,wertarbyte,triggerhappy,$(TRIGGERHAPPY_VERSION))
TRIGGERHAPPY_LICENSE = GPLv3+
TRIGGERHAPPY_LICENSE_FILES = COPYING

define TRIGGERHAPPY_BUILD_CMDS
	$(MAKE) $(TARGET_CONFIGURE_OPTS) \
		LINUX_INPUT_H=$(STAGING_DIR)/usr/include/linux/input.h \
		-C $(@D) thd th-cmd
endef

ifeq ($(BR2_PACKAGE_HAS_UDEV),y)
define TRIGGERHAPPY_INSTALL_UDEV_RULE
	$(TRIGGERHAPPY_FAKEROOT)  $(INSTALL) -D -m 0644 $(@D)/udev/triggerhappy-udev.rules \
		$(TRIGGERHAPPY_TARGET_DIR)/lib/udev/rules.d/triggerhappy.rules
endef
endif

define TRIGGERHAPPY_INSTALL_TARGET_CMDS
	$(TRIGGERHAPPY_FAKEROOT) $(INSTALL) -d $(TRIGGERHAPPY_TARGET_DIR)/etc/triggerhappy/triggers.d
	$(TRIGGERHAPPY_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/thd $(TRIGGERHAPPY_TARGET_DIR)/usr/sbin/thd
	$(TRIGGERHAPPY_FAKEROOT) $(INSTALL) -D -m 0755 $(@D)/th-cmd $(TRIGGERHAPPY_TARGET_DIR)/usr/sbin/th-cmd
	$(TRIGGERHAPPY_FAKEROOT) $(TRIGGERHAPPY_INSTALL_UDEV_RULE)
endef

define TRIGGERHAPPY_INSTALL_INIT_SYSV
	$(TRIGGERHAPPY_FAKEROOT)  $(INSTALL) -m 0755 -D package/triggerhappy/triggerhappy.default \
		$(TRIGGERHAPPY_TARGET_DIR)/etc/default/triggerhappy

	$(TRIGGERHAPPY_FAKEROOT)  $(INSTALL) -m 0755 -D package/triggerhappy/triggerhappy.init \
		$(TRIGGERHAPPY_TARGET_DIR)/etc/init.d/triggerhappy
endef

$(eval $(generic-package))
