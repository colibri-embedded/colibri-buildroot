################################################################################
#
# python-sanic
#
################################################################################
PYTHON_SANIC_CORS_VERSION = 0.9.5
PYTHON_SANIC_CORS_SOURCE = Sanic-Cors-$(PYTHON_SANIC_CORS_VERSION).tar.gz
PYTHON_SANIC_CORS_SITE = https://files.pythonhosted.org/packages/12/33/3d482357f450e9cb24e185aeaa268a0fb084278bd4febc1e165c06072b26
PYTHON_SANIC_CORS_LICENSE = MIT
PYTHON_SANIC_CORS_LICENSE_FILES = LICENSE
PYTHON_SANIC_CORS_SETUP_TYPE = setuptools
PYTHON_SANIC_CORS_DEPENDENCIES = python-sanic python-sanic-plugins-framework

$(eval $(python-package))
