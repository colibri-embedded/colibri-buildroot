################################################################################
#
# colibri-helpers
#
################################################################################

ifeq ($(COLIBRI_LOCAL_DEVELOPMENT),y)
COLIBRI_MANAGER_VERSION=local
COLIBRI_MANAGER_SITE=$(BR2_EXTERNAL)/../colibri-manager
COLIBRI_MANAGER_SITE_METHOD = local
else
#COLIBRI_MANAGER_SITE = $(call github,Colibri-Embedded,earlyboot-utils,$(COLIBRI_MANAGER_VERSION))
#COLIBRI_MANAGER_VERSION = d63101264767b7fbf3f28c57310cbe08d816c4c2
#~ COLIBRI_MANAGER_VERSION = 1.0
#~ COLIBRI_MANAGER_SITE= https://gitlab.com/colibri-embedded/colibri-helpers/repository/v$(COLIBRI_MANAGER_VERSION)/archive.tar.gz
#~ COLIBRI_MANAGER_SOURCE=earlyboot-utils-v$(COLIBRI_MANAGER_VERSION).tar.gz
#~ COLIBRI_MANAGER_DL_OPTS=-O $(COLIBRI_MANAGER_SOURCE)
endif

COLIBRI_MANAGER_LICENSE = GPLv2
COLIBRI_MANAGER_LICENSE_FILES = LICENSE
COLIBRI_MANAGER_DEPENDENCIES = libcurl

define COLIBRI_MANAGER_BUILD_CMDS
	$(MAKE1) -C $(@D) CC="$(TARGET_CC)" DESTDIR=$(COLIBRI_MANAGER_TARGET_DIR)
endef 

define COLIBRI_MANAGER_INSTALL_TARGET_CMDS
	$(COLIBRI_MANAGER_FAKEROOT) -- $(MAKE1) -C $(@D) DESTDIR=$(COLIBRI_MANAGER_TARGET_DIR) install
endef

$(eval $(cmake-package))
