################################################################################
#
# python-sanic-plugins-framework
#
################################################################################
PYTHON_SANIC_PLUGINS_FRAMEWORK_VERSION = 0.6.3.dev20180717
PYTHON_SANIC_PLUGINS_FRAMEWORK_SOURCE = Sanic-Plugins-Framework-$(PYTHON_SANIC_PLUGINS_FRAMEWORK_VERSION).tar.gz
PYTHON_SANIC_PLUGINS_FRAMEWORK_SITE = https://files.pythonhosted.org/packages/f0/8a/4a2a2656b11026f50629a19eca36e48e66a8410314794afb74de242c6d5e
PYTHON_SANIC_PLUGINS_FRAMEWORK_LICENSE = MIT
PYTHON_SANIC_PLUGINS_FRAMEWORK_LICENSE_FILES = LICENSE
PYTHON_SANIC_PLUGINS_FRAMEWORK_SETUP_TYPE = setuptools
PYTHON_SANIC_PLUGINS_FRAMEWORK_DEPENDENCIES = python-sanic

$(eval $(python-package))
