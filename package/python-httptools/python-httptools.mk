################################################################################
#
# python-aiofiles
#
################################################################################
#~ https://files.pythonhosted.org/packages/a3/75/40cdb732e8ef547d9f34ceb83c43ea7188c0ffb719ddc6a1ad160464292d/httptools-0.0.11.tar.gz
PYTHON_HTTPTOOLS_VERSION = 0.0.11
PYTHON_HTTPTOOLS_SOURCE = httptools-$(PYTHON_HTTPTOOLS_VERSION).tar.gz
PYTHON_HTTPTOOLS_SITE = https://files.pythonhosted.org/packages/a3/75/40cdb732e8ef547d9f34ceb83c43ea7188c0ffb719ddc6a1ad160464292d
PYTHON_HTTPTOOLS_LICENSE = Apache-2.0
PYTHON_HTTPTOOLS_LICENSE_FILES = LICENSE
PYTHON_HTTPTOOLS_SETUP_TYPE = setuptools

$(eval $(python-package))
