################################################################################
#
# earlyboot-utils
#
################################################################################

ifeq ($(COLIBRI_LOCAL_DEVELOPMENT),y)
EARLYBOOT_UTILS_VERSION = local
EARLYBOOT_UTILS_SITE=$(BR2_EXTERNAL)/../earlyboot-utils
EARLYBOOT_UTILS_SITE_METHOD = local
else
EARLYBOOT_UTILS_VERSION = d63101264767b7fbf3f28c57310cbe08d816c4c2
EARLYBOOT_UTILS_SITE = $(call github,Colibri-Embedded,earlyboot-utils,$(EARLYBOOT_UTILS_VERSION))
#~ EARLYBOOT_UTILS_VERSION = 1.0
#~ EARLYBOOT_UTILS_SITE= https://gitlab.com/colibri-embedded/earlyboot-utils/repository/v$(EARLYBOOT_UTILS_VERSION)/archive.tar.gz
#~ EARLYBOOT_UTILS_SOURCE=earlyboot-utils-v$(EARLYBOOT_UTILS_VERSION).tar.gz
#~ EARLYBOOT_UTILS_DL_OPTS=-O $(EARLYBOOT_UTILS_SOURCE)
endif

EARLYBOOT_UTILS_LICENSE = GPLv2
EARLYBOOT_UTILS_LICENSE_FILES = LICENSE
EARLYBOOT_UTILS_DEPENDENCIES = libwebsockets

define EARLYBOOT_UTILS_BUILD_CMDS
	$(MAKE1) -C $(@D) CC="$(TARGET_CC)" DESTDIR=$(EARLYBOOT_UTILS_TARGET_DIR)
endef 

define EARLYBOOT_UTILS_INSTALL_TARGET_CMDS
	$(EARLYBOOT_UTILS_FAKEROOT) -- $(MAKE1) -C $(@D) DESTDIR=$(EARLYBOOT_UTILS_TARGET_DIR) install
endef

$(eval $(generic-package))
