BOOTFILES_VERSION ?= v$(shell date +%Y%m%d)
BOOTFILES_PRIORITY ?= 001
BOOTFILES_NAME ?= core
BOOTFILES_FILENAME ?= $(BOOTFILES_PRIORITY)-$(BOOTFILES_NAME)-$(BOOTFILES_VERSION)

BOOTFILES_DEPS := rpi-firmware
ifeq ($(BR2_LINUX_KERNEL),y)
BOOTFILES_DEPS += linux
endif
ifeq ($(BR2_LINUX2_KERNEL),y)
BOOTFILES_DEPS += linux2
endif

bootfiles: colibri-earlyboot $(BOOTFILES_DEPS)
	echo $(BOOTFILES_FILENAME) > $(SDCARD_DIR)/boot_version
	
	rm -rf $(SDCARD_DIR)/bootfiles
	mkdir -p $(SDCARD_DIR)/bootfiles
	
	rm -f $(BINARIES_DIR)/$(BOOTFILES_PRIORITY)-$(BOOTFILES_NAME)-*.zip
	cd $(BOOTFILES_DIR); zip -o $(BINARIES_DIR)/$(BOOTFILES_FILENAME) -r *
